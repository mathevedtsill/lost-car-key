# lost car key 

You could call a locksmith, who can come and make you a new key on the spot. In some cases—an unusual or older vehicle—a [lost car key replacement](https://www.bestmobilelocksmiths.com/) may not be able to help. You might need to buy a new ignition lock cylinder and key from the dealer or an independent repair shop. You can replace your lost car key at a locksmith, even if you don't have the original key. Going straight to a locksmith may be more cost-effective than going to your car dealer, but you'll need some information to make the process easier.
